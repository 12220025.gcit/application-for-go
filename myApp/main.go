package main

import (
	"net/http"

	"fmt"

	"github.com/gorilla/mux"
)

func main() {
	//creattng new router
	router := mux.NewRouter()
	router.HandleFunc("/home", homeHandler)
	err := http.ListenAndServe(":8080", router)
	if err != nil {
		return
	}
}

func homeHandler(w http.ResponseWriter, r *http.Request) {
	_, err := w.Write([]byte("hello world"))
	if err != nil {
		fmt.Println("error: ", err)
	}
}
